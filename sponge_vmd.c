#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "molfile_plugin.h"

static void replace_sponge_name(const char *filename, const char *src, const char *rep, char *result, const char* sp_src, const char* sp_rep)
{
    char dir[256];
    char file2[256];
    char file3[256];
    char *ret, *ret2;
    int strlength;
    memset(dir, 0, sizeof(dir));
    ret = strrchr(filename, '/');
    if (ret == NULL)
    {
        ret = strrchr(filename, '\\');
    }
    if (ret != NULL)
    {
        ret += 1;
        if (sp_src != NULL && strcmp(ret, sp_src) == 0)
        {
            strlength = strlen(filename) - strlen(ret);
            strncpy(dir, filename, strlength);
            strcat(dir, sp_rep);
            strcpy(result, dir);
            return;
        }
        else
        {
            strlength = strlen(filename) - strlen(ret);
            strncpy(dir, filename, strlength);
            strcpy(file2, ret);
        }

    }
    else
    {
        strcpy(file2, filename);
        strcpy(dir, "./");
    }
    ret2 = strstr(file2, src);
    if ( ret2 != NULL)
    {
        memset(file3, 0, sizeof(file3));
        strncpy(file3, file2, strlen(file2) - strlen(ret2));
        strcat(file3, rep);
        strlength = strlen(src);
        strcat(file3, ret2 + strlength);
        strcat(dir, file3);
        strcpy(result, dir);
    }
    else
    {
        result[0] = 0;
    }
    return;
}

typedef struct {
    FILE *fp;
    FILE *box;
} sponge_traj_data;

static void *open_sponge_traj_read(const char *filename, const char *filetype, int *natoms) {
    sponge_traj_data *data;
    data = (sponge_traj_data *)malloc(sizeof(sponge_traj_data));
    data->box = NULL;
    data->fp = NULL;
    data->fp = fopen(filename, "rb");
    if (data->fp == NULL)
    {
        fprintf(stderr, "SpongeError) Error in opening sponge trajectory file '%s'\n", filename);
        return NULL;
    }
    printf("SpongeInfo) reading sponge trajectory file '%s'\n", filename);
    char box_filename[256];
    replace_sponge_name(filename, "dat", "box", box_filename, "mdcrd.dat", "mdbox.txt");
    if (strlen(box_filename) == 0)
    {
        printf("SpongeInfo) No keyword 'dat' in '%s'. Box will not be read\n", filename);
    }
    else
    {
        data->box = fopen(box_filename, "r");
        if (data->box == NULL)
        {
            printf("SpongeInfo) '%s' Not found. Box will not be read\n", box_filename);
        }
        else
        {
            printf("SpongeInfo) reading sponge box file '%s'\n", box_filename);
        }
    }
    *natoms = MOLFILE_NUMATOMS_UNKNOWN;  
    return data;
}

static int read_sponge_traj_timestep(void *mydata, int natoms, molfile_timestep_t *ts)
{
    sponge_traj_data *data= (sponge_traj_data *)mydata;
    int success = 1;
    float* temp_crd;
    float* temp_A, *temp_B, *temp_C, *temp_alpha, *temp_beta, *temp_gamma;
    if (ts == NULL)
    {
        temp_crd = (float*)malloc(sizeof(float) * 3 * natoms);
        temp_A = (float*)malloc(sizeof(float));
        temp_B = (float*)malloc(sizeof(float));
        temp_C = (float*)malloc(sizeof(float));
        temp_alpha = (float*)malloc(sizeof(float));
        temp_beta = (float*)malloc(sizeof(float));
        temp_gamma = (float*)malloc(sizeof(float));
    }
    else
    {
        temp_crd = ts->coords;
        temp_A = &(ts->A);
        temp_B = &(ts->B);
        temp_C = &(ts->C);
        temp_alpha = &(ts->alpha);
        temp_beta = &(ts->beta);
        temp_gamma = &(ts->gamma);
    }
    if (fread(temp_crd, sizeof(float), 3*natoms, data->fp) != 3*natoms)
        success = 0;
    if (data->box != NULL)
    {
        if (fscanf(data->box, "%f %f %f %f %f %f", temp_A, temp_B, temp_C,
            temp_alpha, temp_beta, temp_gamma)  != 6)
        {
            temp_A[0] = 0;
            temp_B[0] = 0;
            temp_C[0] = 0;
            temp_alpha[0] = 90;
            temp_beta[0] = 90;
            temp_gamma[0] = 90;
        }
    }
    if (ts == NULL)
    {
        free(temp_crd);
        free(temp_A);
        free(temp_B);
        free(temp_C);
        free(temp_alpha);
        free(temp_beta);
        free(temp_gamma);
    }
    return success ? MOLFILE_SUCCESS : MOLFILE_ERROR;
}

static void close_sponge_traj_read(void *mydata) {
  sponge_traj_data *data= (sponge_traj_data *)mydata;
  fclose(data->fp);
  if (data->box != NULL)
    fclose(data->box);
  free(data);
}

typedef struct {
    FILE *fp;
    int atom_numbers;
    char filename[256];
    int finished;
} sponge_crd_data;

static void *open_sponge_crd_read(const char *filename, const char *filetype, int *natoms) {
    sponge_crd_data *data;
    data = (sponge_crd_data *)malloc(sizeof(sponge_crd_data));
    data->fp = NULL;
    data->fp = fopen(filename, "r");
    if (data->fp == NULL)
    {
        fprintf(stderr, "SpongeError) Error in openning sponge coordinate file '%s'\n", filename);
        return NULL;
    }
    char buffer[256];
    fgets(buffer,256, data->fp);
    if (sscanf(buffer, "%d", &data->atom_numbers) != 1)
    {
        fprintf(stderr, "SpongeError) Error in reading atom_numbers in sponge coordinate file '%s'\n", filename);
        return NULL;
    }
    *natoms = data->atom_numbers;
    strcpy(data->filename, filename);
    data->finished = 0;
    return data;
}

static int read_sponge_crd_timestep(void *mydata, int natoms, molfile_timestep_t *ts) {
    sponge_crd_data *data= (sponge_crd_data *)mydata;
    if (data->finished)
        return MOLFILE_EOF;
    int i;
    for (i = 0; i < data->atom_numbers; i++)
    {
        int toret = fscanf(data->fp, "%f %f %f", &ts->coords[3*i], &ts->coords[3*i + 1], &ts->coords[3*i + 2]);
        if (toret != 3)
        {
            fprintf(stderr, "SpongeError) Error in reading atom coordinates in sponge coordinate file '%s'\n", data->filename);
            return MOLFILE_ERROR;
        }
    }
    if (fscanf(data->fp, "%f %f %f %f %f %f", &ts->A, &ts->B, &ts->C, &ts->alpha, &ts->beta, &ts->gamma) != 6)
    {
        fprintf(stderr, "SpongeError) Error in reading box in sponge coordinate file '%s'\n", data->filename);
        return MOLFILE_ERROR;
    }
    data->finished = 1;
    return MOLFILE_SUCCESS;
}

static void close_sponge_crd_read(void *mydata) {
  sponge_crd_data *data= (sponge_crd_data *)mydata;
  fclose(data->fp);
  free(data);
}

typedef struct {
    FILE *fp;
    int atom_numbers;
    float *mass;
    FILE *bond;
    FILE *atom_name;
    FILE *atom_type_name;
    FILE *resname;
    FILE *residue;
    int *from;
    int *to;
} sponge_mass_data;


static void *open_sponge_mass_read(const char *filename, const char *filetype, int *natoms) {
    sponge_mass_data *data;

    data = (sponge_mass_data *)malloc(sizeof(sponge_mass_data));
    data->bond = NULL;
    data->atom_name = NULL;
    data->atom_type_name = NULL;
    data->resname = NULL;
    data->residue = NULL;
    data->fp = NULL;
    data->fp = fopen(filename, "r");
    int tempint = 0;
    if (data->fp == NULL)
    {
        fprintf(stderr, "SpongeError) Error in opening sponge mass file '%s'\n", filename);
        return NULL;
    }
    printf("SpongeInfo) reading sponge mass file '%s'\n", filename);
    if (fscanf(data->fp, "%d", natoms) != 1)
    {
        fprintf(stderr, "SpongeError) Error in reading atom_numbers in sponge mass file '%s'\n", filename);
        return NULL;
    }
    data->atom_numbers = *natoms;
    data->mass = (float *)malloc(sizeof(float)*data->atom_numbers);
    int i;
    for (i = 0; i < data->atom_numbers; i++)
    {
        if (fscanf(data->fp, "%f", data->mass + i) != 1)
        {
            fprintf(stderr, "SpongeError) Error in reading atom mass in sponge mass file '%s'\n", filename);
            return NULL;
        }
    }
    char temp_filename[256];
    replace_sponge_name(filename, "mass", "bond", temp_filename, NULL, NULL);
    if (strlen(temp_filename) == 0)
    {
        printf("SpongeInfo)No keyword 'mass' in '%s'. Bond will not be read\n", filename);
    }
    else
    {
        data->bond = fopen(temp_filename, "r");
        if (data->bond == NULL)
        {
            printf("SpongeInfo)'%s' Not found. Bond will not be read\n", temp_filename);
        }
        else
        {
            printf("SpongeInfo) reading sponge bond file '%s'\n", temp_filename);
        }
    }
    replace_sponge_name(filename, "mass", "residue", temp_filename, NULL, NULL);
    if (strlen(temp_filename) == 0)
    {
        printf("SpongeInfo)No keyword 'mass' in '%s'. Residue index will not be read and will be guessed\n", filename);
    }
    else
    {
        data->residue = fopen(temp_filename, "r");
        if (data->residue == NULL)
        {
            printf("SpongeInfo)'%s' Not found. Residue index will not be read and will be guessed\n", temp_filename);
        }
        else
        {
            printf("SpongeInfo) reading sponge residue file '%s'\n", temp_filename);
        }
    }
    if (data->residue != NULL)
    {
        if (fscanf(data->residue, "%d %*d", &tempint) != 1)
        {
            printf("SpongeError) Error in reading residue numbers in sponge residue file\n");
            fclose(data->residue);
            data->residue = NULL;
        }
        if (tempint != data->atom_numbers)
        {
            printf("SpongeError) atom_numbers in sponge residue file (%d) != (%d) atom numbers in sponge mass file\n", tempint, data->atom_numbers);
            fclose(data->residue);
            data->residue = NULL;
        }
    }
    replace_sponge_name(filename, "mass", "resname", temp_filename, NULL, NULL);
    if (strlen(temp_filename) == 0)
    {
        printf("SpongeInfo)No keyword 'mass' in '%s'. Residue name will not be read and will be guessed\n", filename);
    }
    else
    {
        data->resname = fopen(temp_filename, "r");
        if (data->resname == NULL)
        {
            printf("SpongeInfo)'%s' Not found. Residue name will not be read and will be guessed\n", temp_filename);
        }
        else
        {
            printf("SpongeInfo) reading sponge resname file '%s'\n", temp_filename);
        }
    }
    if (data->resname != NULL)
    {
        if (fscanf(data->resname, "%d", &tempint) != 1)
        {
            printf("SpongeError) Error in reading resname numbers in sponge resname file\n");
            fclose(data->resname);
            data->resname = NULL;
        }
    }
    replace_sponge_name(filename, "mass", "atom_name", temp_filename, NULL, NULL);
    if (strlen(temp_filename) == 0)
    {
        printf("SpongeInfo)No keyword 'mass' in '%s'. Atom name will not be read and will be guessed\n", filename);
    }
    else
    {
        data->atom_name = fopen(temp_filename, "r");
        if (data->atom_name == NULL)
        {
            printf("SpongeInfo)'%s' Not found. Atom name will not be read and will be guessed\n", temp_filename);
        }
        else
        {
            printf("SpongeInfo) reading sponge atom name file '%s'\n", temp_filename);
        }
    }
    if (data->atom_name != NULL)
    {
        if (fscanf(data->atom_name, "%d", &tempint) != 1)
        {
            printf("SpongeError) Error in reading atom_numbers in sponge atom name file\n");
            fclose(data->atom_name);
            data->atom_name = NULL;
        }
        if (tempint != data->atom_numbers)
        {
            printf("SpongeError) atom_numbers in sponge atom name file (%d) != (%d) atom numbers in sponge mass file\n", tempint, data->atom_numbers);
            fclose(data->atom_name);
            data->atom_name = NULL;
        }
    }
    replace_sponge_name(filename, "mass", "atom_type_name", temp_filename, NULL, NULL);
    if (strlen(temp_filename) == 0)
    {
        printf("SpongeInfo)No keyword 'mass' in '%s'. Atom type will not be read and will be guessed\n", filename);
    }
    else
    {
        data->atom_type_name = fopen(temp_filename, "r");
        if (data->atom_type_name == NULL)
        {
            printf("SpongeInfo)'%s' Not found. Atom type will not be read and will be guessed\n", temp_filename);
        }
        else
        {
            printf("SpongeInfo) reading sponge atom type file '%s'\n", temp_filename);
        }
    }
    if (data->atom_type_name != NULL)
    {
        if (fscanf(data->atom_type_name, "%d", &tempint) != 1)
        {
            printf("SpongeError) Error in reading atom_numbers in sponge atom type name file\n");
            fclose(data->atom_type_name);
            data->atom_type_name = NULL;
        }
        if (tempint != data->atom_numbers)
        {
            printf("SpongeError) atom_numbers in sponge atom type name file (%d) != (%d) atom numbers in sponge mass file\n", tempint, data->atom_numbers);
            fclose(data->atom_type_name);
            data->atom_type_name = NULL;
        }
    }
    return data;
}

/* periodic table of elements for translation of ordinal to atom type */
static const char *pte_label[] = { 
    "X",  "H",  "He", "Li", "Be", "B",  "C",  "N",  "O",  "F",  "Ne",
    "Na", "Mg", "Al", "Si", "P" , "S",  "Cl", "Ar", "K",  "Ca", "Sc",
    "Ti", "V",  "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", 
    "As", "Se", "Br", "Kr", "Rb", "Sr", "Y",  "Zr", "Nb", "Mo", "Tc",
    "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn", "Sb", "Te", "I",  "Xe",
    "Cs", "Ba", "La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb",
    "Dy", "Ho", "Er", "Tm", "Yb", "Lu", "Hf", "Ta", "W",  "Re", "Os",
    "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr",
    "Ra", "Ac", "Th", "Pa", "U",  "Np", "Pu", "Am", "Cm", "Bk", "Cf",
    "Es", "Fm", "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt",
    "Ds", "Rg"
};
static const int nr_pte_entries = sizeof(pte_label) / sizeof(char *);

/* corresponding table of masses. */
static const float pte_mass[] = { 
    /* X  */ 0.00000, 1.00794, 4.00260, 6.941, 9.012182, 10.811,  
    /* C  */ 12.0107, 14.0067, 15.9994, 18.9984032, 20.1797, 
    /* Na */ 22.989770, 24.3050, 26.981538, 28.0855, 30.973761,
    /* S  */ 32.065, 35.453, 39.948, 39.0983, 40.078, 44.955910,
    /* Ti */ 47.867, 50.9415, 51.9961, 54.938049, 55.845, 58.9332,
    /* Ni */ 58.6934, 63.546, 65.409, 69.723, 72.64, 74.92160, 
    /* Se */ 78.96, 79.904, 83.798, 85.4678, 87.62, 88.90585, 
    /* Zr */ 91.224, 92.90638, 95.94, 98.0, 101.07, 102.90550,
    /* Pd */ 106.42, 107.8682, 112.411, 114.818, 118.710, 121.760, 
    /* Te */ 127.60, 126.90447, 131.293, 132.90545, 137.327, 
    /* La */ 138.9055, 140.116, 140.90765, 144.24, 145.0, 150.36,
    /* Eu */ 151.964, 157.25, 158.92534, 162.500, 164.93032, 
    /* Er */ 167.259, 168.93421, 173.04, 174.967, 178.49, 180.9479,
    /* W  */ 183.84, 186.207, 190.23, 192.217, 195.078, 196.96655, 
    /* Hg */ 200.59, 204.3833, 207.2, 208.98038, 209.0, 210.0, 222.0, 
    /* Fr */ 223.0, 226.0, 227.0, 232.0381, 231.03588, 238.02891,
    /* Np */ 237.0, 244.0, 243.0, 247.0, 247.0, 251.0, 252.0, 257.0,
    /* Md */ 258.0, 259.0, 262.0, 261.0, 262.0, 266.0, 264.0, 269.0,
    /* Mt */ 268.0, 271.0, 272.0
};

static int read_sponge_mass(void *mydata, int *optflags, molfile_atom_t *atoms) {
    sponge_mass_data *p = (sponge_mass_data *)mydata;

    *optflags = MOLFILE_MASS | MOLFILE_ATOMICNUMBER;
    float mass;
    int index;
    int i;
    int tempint;
    char tempchar[256];
    int scanf_ret;
    int errored = 0;
    int find = 0;
    int rescount = -1;
    int last_residue = -1;
    char last_resname[8];
    last_resname[0] = 0;
    for ( i = 0; i < p->atom_numbers; i++)
    {
        mass = p->mass[i];
        atoms[i].mass = mass;
        //氢和氘
        if ( mass > 0.0 && mass < 2.2) 
        {
                index = 1;
        }
        //铋和钋
        else if ( mass > 207.85 && mass < 208.99) 
        {
                index = 83;
        }
        //镍和钴
        else if ( mass > 56.50 && mass < 58.8133) 
        {
                index = 27;
        }
        //钾和氩
        else if (mass > 38.5 && mass < 39.6)
        {
                index = 19;
        }
        //在列表里找
        else
        {
            index = 0;
            for (int j = 0; j < 111; j++)
            {
                if ( abs(mass - pte_mass[j]) < 0.65)
                {
                    index = j;
                    break;
                }
            }
        }
        find = 0;
        if (p->atom_type_name != NULL)
        {
            if (fscanf(p->atom_type_name, "%s", tempchar) == 1)
            {
                memset(atoms[i].type, 0, 16);
                strncpy(atoms[i].type, tempchar, 16);
                find = 1;
            }
            else if (errored < 5)
            {
                fprintf(stderr, "SpongeError) Error in reading type name of atom #%d, it will be guessed\n", i);
                errored += 1;
            }
            else if (errored == 5)
            {
                fprintf(stderr, "SpongeError) Too many error occured. No more error information will be printed\n");
                errored += 1;
            }
        }
        if (!find)
        {
            memset(atoms[i].type, 0, 16);
            strncpy(atoms[i].type, pte_label[index], 16);
        }
        find = 0;
        if (p->atom_name != NULL)
        {
            if (fscanf(p->atom_name, "%s", tempchar) == 1)
            {
                memset(atoms[i].name, 0, 16);
                strncpy(atoms[i].name, tempchar, 16);
                find = 1;
            }
            else if (errored < 5)
            {
                fprintf(stderr, "SpongeError) Error in reading name of atom #%d, it will be guessed\n", i);
                errored += 1;
            }
            else if (errored == 5)
            {
                fprintf(stderr, "SpongeError) Too many error occured. No more error information will be printed\n");
                errored += 1;
            }
        }
        if (!find)
        {
            memset(atoms[i].name, 0, 16);
            strncpy(atoms[i].name, pte_label[index], 16);
        }
        if (rescount < i)
        {
            if (p->residue != NULL && last_residue != -2)
            {
                if (fscanf(p->residue, "%d", &tempint) == 1)
                {
                    rescount += tempint;
                    last_residue += 1;
                }
                else
                {
                    fprintf(stderr, "SpongeError) Error in reading residue index of atom #%d", i);
                    last_residue = -2;
                }
            }
            if (p->resname != NULL && last_residue >= 0)
            {
                if (fscanf(p->resname, "%s", tempchar) == 1)
                {
                    memset(atoms[i].resname, 0, 8);
                    strncpy(atoms[i].resname, tempchar, 8);
                    strncpy(last_resname, tempchar, 8);
                }
                else
                {
                    fprintf(stderr, "SpongeError) Error in reading residue name of atom #%d", i);
                    last_residue = -2;
                }
            }
        }
        if (last_residue >= 0)
        {
            atoms[i].resid = last_residue;
            strncpy(atoms[i].resname, last_resname, 8);
        }
        atoms[i].atomicnumber = index;
        atoms[i].chain[0] = 0;
        atoms[i].segid[0] = 0;
    }

    return MOLFILE_SUCCESS;
}

static int read_sponge_bonds(void *v, int *nbonds, int **fromptr, int **toptr, 
                            float **bondorderptr, int **bondtype, 
                            int *nbondtypes, char ***bondtypename){
    sponge_mass_data *data = (sponge_mass_data *)v;
    data->from = NULL;
    data->to = NULL;
    *nbonds = 0;
    if (data->bond != NULL)
    {
        fscanf(data->bond, "%d", nbonds);
        data->from = (int*)malloc(sizeof(int) * *nbonds);
        data->to = (int*)malloc(sizeof(int) * *nbonds);
        
        int i;
        for (i = 0; i < *nbonds; i++)
        {
            fscanf(data->bond, "%d %d %*f %*f", data->from + i, data->to + i);
            data->from[i] += 1;
            data->to[i] += 1;
        }
        *fromptr = data->from;
        *toptr = data->to;
    }
    *bondorderptr = NULL;
    *bondtype = NULL;
    *nbondtypes = 0;
    *bondtypename = NULL;
    return MOLFILE_SUCCESS;
}

static void close_sponge_mass_read(void *mydata) {
    sponge_mass_data *data= (sponge_mass_data *)mydata;
    free(data->mass);
    fclose(data->fp);
    if (data->bond != NULL)
        fclose(data->bond);
    if (data->atom_name != NULL)
        fclose(data->atom_name);
    if (data->atom_type_name != NULL)
        fclose(data->atom_type_name);
    if (data->resname != NULL)
        fclose(data->resname);
    if (data->residue != NULL)
        fclose(data->residue);
    if (data->from != NULL)
        free(data->from);
    if (data->to != NULL)
        free(data->to);
    free(data);
}
static molfile_plugin_t plugin;
static molfile_plugin_t plugin2;
static molfile_plugin_t plugin3;

VMDPLUGIN_API int VMDPLUGIN_init(void)
{
    memset(&plugin, 0, sizeof(molfile_plugin_t));
    plugin.abiversion = vmdplugin_ABIVERSION;
    plugin.type = MOLFILE_PLUGIN_TYPE;
    plugin.name = "sponge_traj";
    plugin.prettyname = "SPONGE trajectory file";
    plugin.author = "Yijie Xia";
    plugin.majorv = 1;
    plugin.minorv = 3;
    plugin.is_reentrant = VMDPLUGIN_THREADSAFE;
    plugin.filename_extension = "dat";
    plugin.open_file_read = open_sponge_traj_read;
    plugin.read_next_timestep = read_sponge_traj_timestep;
    plugin.close_file_read = close_sponge_traj_read;
    
    memset(&plugin2, 0, sizeof(molfile_plugin_t));
    plugin2.abiversion = vmdplugin_ABIVERSION;
    plugin2.type = MOLFILE_PLUGIN_TYPE;
    plugin2.name = "sponge_crd";
    plugin2.prettyname = "SPONGE coordinate file";
    plugin2.author = "Yijie Xia";
    plugin2.majorv = 1;
    plugin2.minorv = 3;
    plugin2.is_reentrant = VMDPLUGIN_THREADSAFE;
    plugin2.filename_extension = "crd";
    plugin2.open_file_read = open_sponge_crd_read;
    plugin2.read_next_timestep = read_sponge_crd_timestep;
    plugin2.close_file_read = close_sponge_crd_read;
    
    memset(&plugin3, 0, sizeof(molfile_plugin_t));
    plugin3.abiversion = vmdplugin_ABIVERSION;
    plugin3.type = MOLFILE_PLUGIN_TYPE;
    plugin3.name = "sponge_mass";
    plugin3.prettyname = "SPONGE mass information file";
    plugin3.author = "Yijie Xia";
    plugin3.majorv = 1;
    plugin3.minorv = 3;
    plugin3.is_reentrant = VMDPLUGIN_THREADSAFE;
    plugin3.filename_extension = "mass";
    plugin3.open_file_read = open_sponge_mass_read;
    plugin3.read_structure = read_sponge_mass;
    plugin3.read_bonds = read_sponge_bonds;
    plugin3.close_file_read = close_sponge_mass_read;
    return VMDPLUGIN_SUCCESS; 
}

VMDPLUGIN_API int VMDPLUGIN_register(void *v, vmdplugin_register_cb cb) {
  (*cb)(v, (vmdplugin_t *)&plugin);
  (*cb)(v, (vmdplugin_t *)&plugin2);
  (*cb)(v, (vmdplugin_t *)&plugin3);
  return VMDPLUGIN_SUCCESS;
}

VMDPLUGIN_API int VMDPLUGIN_fini(void) { return VMDPLUGIN_SUCCESS; }