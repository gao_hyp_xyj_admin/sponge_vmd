使用步骤：
1. 编译动态库(Windows下可跳过)
2. 移动文件
3. 使用VMD

注意确认VMD版本和位数！推荐64位1.9.4

1. 编译动态库
通常来说，LINUX系统下编译命令为
gcc -shared sponge_vmd.c -o sponge_vmd.so -fPIC
注意，本压缩包内的molfile_plugin.h和vmdplugin.h是基于VMD最新版1.9.4的，其他版需要自行将
"你的vmd安装地址/plugins/include"中对应的文件复制至编译文件夹中
include文件夹与下面的molfile文件夹在同一文件夹，如果找不到可以用相同方法定位

如果使用WINDOWS系统，也可以类似LINUX自行编译，也可直接使用本压缩包自带的已编译好文件，其中文件名与VMD对应是：
sponge_vmd_stable_WIN32.so		32位VMD1.9.0~1.9.3
sponge_vmd_alpha_WIN64.so		64位VMD1.9.4
sponge_vmd_alpha_WIN32.so		32位VMD1.9.4

sponge_vmd_alpha_WIN64.so是最新版本，其他32位版本动态库.so因为源码丢失而未继续更新，可能存在潜在bug。

2. 移动文件
将上一步生成的sponge_vmd.so复制到
你的vmd安装地址/plugins/你的操作系统架构/molfile
这个文件夹的地址可能会根据你的VMD安装不同而不同，可在打开VMD的命令行里观察，例如我的电脑上有：
"
Info) Dynamically loaded 79 plugins in directory:
Info) C:/Program Files/VMD/plugins/WIN64/molfile
"

3. 使用VMD
将vmd加入环境变量，可以在命令行里或图形化界面加载质量文件（附带bond文件）、坐标文件、轨迹文件（附带周期性盒子文件）
①读质量文件：
vmd -sponge_mass 质量文件名 (读取坐标/轨迹文件命令)
如果在同一文件夹下有质量文件名中"mass"替换为"bond"的文件，将会读取相关的成键信息
②读坐标文件：
vmd (读取拓扑文件命令) -sponge_crd 坐标文件名
③读轨迹文件
vmd (读取拓扑文件命令) -sponge_traj 轨迹文件名
如果轨迹文件名的后缀是.dat，那么可以省略"-sponge_traj"自动通过后缀识别
如果轨迹文件名为mdcrd.dat且同一文件夹下有mdbox.txt，将会读取相关的周期性边界信息
如果轨迹文件名的后缀是.dat，且同一文件夹内有相同文件名后缀为.box的文件，将会读取相关的周期性边界信息

